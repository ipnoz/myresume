<?php

namespace Page;

/**
 * Class ContactEmail
 *
 * Helper for the contact page.
 */
class ContactEmail
{
    /**
     * Contact page URN location
     */
    const location = '/';

    /**
     * Location of the action script for functional tests
     */
    const POST_location = '/ContactEmail.php';

    /**
     * The contat form HTML element locator
     */
    const formEl = 'form#contact';

    /**
     * Contact fields names
     */
    const fieldName = 'name';
    const fieldEmail = 'email';
    const fieldTelephone = 'telephone';
    const fieldSubject = 'subject';
    const fieldMessage = 'message';

    /**
     * Contact submit form button locator
     */
    const submitButton = self::formEl.' button[type=submit]';

    /**
     * Datas to populate tests emails
     */
    const testName = 'testName';
    const testEmail = 'test@test.tld';
    const testPhoneNumber = '+ 9 12 34 56 78 910';
    const testSubject = 'testSubject';
    const testMessage =
        'Test message: hello tester ! <br/> <strong>Test HTML strip tags in message</strong>
        Test UTF8:
        <i>test French accent in message àéèùç </i>
        <div>test other $^%*µ!§# < / \ > </div>';
}

