<?php

use Page\ContactEmail as Email;

/**
 * Class contactEmailJSCest
 *
 * Test to send a contact email with the jQuery AJAX function
 *
 * Run "bin/codecept run -x skip" to skip the skip group ^_^
 * @group skip
 * @group email
 * @group ajax
 */
class contactEmailJSCest
{
    public function send_contact_email_with_ajax_must_works(AcceptanceTester $I)
    {
        $I->amOnPage(Email::location);

        $I->waitForElementNotVisible('#preloader', 10);

        $I->click('a[data-target="#id49735168423647"]');
        $I->waitForElementClickable(Email::formEl, 10);

        $I->fillField(Email::fieldName, Email::testName);
        $I->fillField(Email::fieldEmail, Email::testEmail);
        $I->fillField(Email::fieldTelephone, Email::testPhoneNumber);
        $I->fillField(Email::fieldSubject, Email::testSubject.' (AJAX)');
        $I->fillField(Email::fieldMessage, Email::testMessage);

        $I->click(Email::submitButton);

        $I->waitForJS('return $.active == 0;', 30);
        $I->seeElement('.alert.alert-success');
    }
}
