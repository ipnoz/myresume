<?php

use Page\ContactEmail as Email;

/**
 * Class contactEmailCest
 *
 * Test for sending an email works, or fail if form is invalid
 *
 * Run "bin/codecept run -x skip" to skip the skip group ^_^
 * @group skip
 * @group email
 */
class contactEmailCest
{
    public function send_contact_email_with_all_field_must_works(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => Email::testName,
                'email' => Email::testEmail,
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('success');
    }

    public function send_contact_email_without_hidden_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'name' => Email::testName,
                'email' => Email::testEmail,
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_without_name_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'email' => Email::testEmail,
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_without_email_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => Email::testName,
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_without_telephone_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => Email::testName,
                'email' => Email::testEmail,
                'subject' => Email::testSubject,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_without_subject_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => Email::testName,
                'email' => Email::testEmail,
                'telephone' => Email::testPhoneNumber,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_without_message_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => Email::testName,
                'email' => Email::testEmail,
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_with_empty_name_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => '',
                'email' => Email::testEmail,
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_with_empty_email_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => Email::testName,
                'email' => '',
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject,
                'message' => Email::testMessage
            ]
        );
        $I->canSeeResponseEquals('error');
    }

    public function send_contact_email_with_empty_message_field_must_fail(FunctionalTester $I)
    {
        $I->sendPOST(
            Email::POST_location,
            [
                'cmd' => 'send_email_contact',
                'name' => Email::testName,
                'email' => Email::testEmail,
                'telephone' => Email::testPhoneNumber,
                'subject' => Email::testSubject,
                'message' => ''
            ]
        );
        $I->canSeeResponseEquals('error');
    }
}
