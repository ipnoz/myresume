
let gulp = require('gulp');
let gutil = require('gulp-util');
let del = require('del');
let browserSync = require('browser-sync').create();
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let rename = require("gulp-rename");
let uglify = require('gulp-uglify');
let preservetime = require('gulp-preservetime');

let SRC = './src';
let DEST = './dist';

const HTTP_PATH = '/var/www/html';
const START_PATH = __dirname.replace(HTTP_PATH, '')+'/'+DEST;

const paths = {
    css: {
        src: [SRC+'/scss/**/*.scss'],
        dest: DEST+'/css'
    },
    js: {
        src: [SRC+'/js/**/*.js'],
        dest: DEST+'/js'
    },
    images: {
        src: [SRC+'/img/**/*'],
        dest: DEST+'/img'
    },
    html: {
        src: [SRC+'/*.html'],
        dest: DEST+'/'
    },
    php: {
        src: [SRC+'/*.php'],
        dest: DEST+'/'
    }
};

gulp.task('clean', function () {
    return del([DEST]);
});

// vendor
gulp.task('vendor', function() {
    gulp.src([SRC+'/vendor/*/*.min.*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/fonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/fonts'))
        .pipe(preservetime());
    return gulp.src([SRC+'/vendor/webfonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/webfonts'))
        .pipe(preservetime());
});

// HTML
gulp.task('html', function () {
    return gulp.src(paths.html.src)
        .pipe(gulp.dest(paths.html.dest))
        .pipe(browserSync.stream());
});

// php
gulp.task('php', function () {
    return gulp.src(paths.php.src)
        .pipe(gulp.dest(paths.php.dest))
        .pipe(browserSync.stream());
});

// css
function compileScss() {
    return gulp.src(paths.css.src)
        .pipe(sass.sync({outputStyle:'compressed'}).on('error', sass.logError))
        .pipe(cleanCSS({inline: 'none'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.css.dest))
        .pipe(browserSync.stream());
}

gulp.task('css', gulp.series(compileScss));

// js
function compileJs() {
    return gulp.src(paths.js.src)
        .pipe(uglify().on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.js.dest))
        .pipe(browserSync.stream());
}

gulp.task('js', gulp.series(compileJs));

// image
gulp.task('img', function () {
    return gulp.src(paths.images.src)
        .pipe(gulp.dest(paths.images.dest))
        .pipe(preservetime());
});

function browserSyncDaemon() {
    browserSync.init({
        proxy: '172.18.0.1',
        open: false,
        notify: false,
        logFileChanges: false,
        reloadOnRestart: true,
        ghostMode: false,
        startPath: START_PATH
    });
}

function watchFiles() {
    gulp.watch(paths.html.src, gulp.series('html'));
    gulp.watch(paths.php.src, gulp.series('php'));
    gulp.watch(paths.css.src, gulp.series('css'));
    gulp.watch(paths.js.src, gulp.series('js'));
}

gulp.task('default', gulp.parallel(browserSyncDaemon, watchFiles));
gulp.task('build', gulp.series('vendor', 'html', 'php', 'css', 'js', 'img'));
gulp.task('rebuild', gulp.series('clean', 'build'));
