(function($) {

    "use strict";

    var menuEl = 'nav #menu';
    var menuButtonEl = 'nav button';
    var menuToggleTime = 250;

    /**
     * Toggle responsive menu
     *
     * Override jQuery.toggle() method action:
     * Remove style="display:[block|none]" injected into the HTML element
     * and add css class "show"
     */
    function toggleMenu() {
        $(menuEl).toggle(menuToggleTime, function() {
            $(this).css('display', '').toggleClass('show');
        });
    }

    function closeMenu() {
        $(menuEl).hide(menuToggleTime, function() {
            $(this).css('display', '').removeClass('show');
        });
    }

    /**
     * Action button: toggle responsive menu
     */
    $(menuButtonEl).on('click', toggleMenu);

    /**
     * js-scroll-trigger event
     */
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        /**
         * Reset scrollTop animations before run another one.
         * Avoid to queue many animations on multiple click and be stucked waiting for them to finish.
         */
        $('html, body').stop();

        /**
         * Closes responsive menu when a scroll trigger link is clicked
         */
        if (window.innerWidth < 992) {
            closeMenu();
        }

        /**
         * Smooth scrolling on all links inside the navbar
         */
        if (this.hash !== '') {
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, 'easeInOutExpo');
            return false;
        }
    });

    /**
     * If available, activate Bootstrap.Scrollspy to add .active class on navbar items during scroll.
     */
    if (typeof $().scrollspy == 'function') {
        $('body').scrollspy({
            target: menuEl
        });
    }

    /**
     * Activate AOS.js
     */
    if (typeof AOS == 'object') {
        AOS.init({
            duration: '1250'
            // disable: 'mobile',
            // disable: true,
            // offset: 300,
        });
    }

    /**
     * Contact email with AJAX
     */

    var contactEl = $('form#contact');

    // Allow one submit action
    var contactFormSubmitted = false;

    // display only one warning message
    var warningMessageSubmitted = false;

    $(contactEl).submit(function() {

        /**
         * Display bootstrap alert
         */
        function displayAlert(css, message) {
            $(contactEl.find('.alert-resume')).removeClass('alert-danger alert-warning alert-success').addClass('alert alert-'+css).text(message);
        }

        var data = {
            // Only send email with this AJAX data
            cmd: 'send_email_contact',
            name: $(contactEl.find('input[name="name"]')).val(),
            email: $(contactEl.find('input[name="email"]')).val(),
            telephone: $(contactEl.find('input[name="telephone"]')).val(),
            subject: $(contactEl.find('input[name="subject"]')).val(),
            message: $(contactEl.find('textarea[name="message"]')).val(),
        };

        if (!contactFormSubmitted) {

            $.ajax({
                type: 'POST',
                url: 'ContactEmail.php',
                data: data,
                success: function(response) {
                    if (response === 'success') {
                        displayAlert('success', 'Le message a bien été envoyé');
                        contactFormSubmitted = true;
                    } else if (response === 'error') {
                        displayAlert('danger', 'Erreur: le message n\'a pas été envoyé');
                    } else {
                        displayAlert('danger', response);
                    }
                },
                error: function(request, status, error) {
                    alert('AJAX Error: '+request.responseText);
                }
            });
        } else {
            if (!warningMessageSubmitted) {
                displayAlert('warning', 'Un seul message à la fois. Veuillez recharger la page');
                warningMessageSubmitted = true;
            }
        }

        return false;
    });

    /*
     * Writing citation letters
     */
    var citation = $('#principal-quote q');

    var showText = function (target, message, index, interval) {
        if (index < message.length) {
            $(target).append(message[index++]);
            setTimeout(function () { showText(target, message, index, interval); }, interval);
        }
    };

    var text = citation.text();
    citation.text('');

    showText(citation, text, 0, 150);

})(jQuery);
