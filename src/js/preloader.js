(function ($) {

    'use strict';

    var preloaderEl = '#preloader';

    /**
     * Preloader
     */
    $(window).on('load', function () {
        $(preloaderEl).fadeOut('slow', function () {
            $(this).remove();
        });
    });

    /**
     * Preloader
     * Set a timeout of 3 secondes to avoid to long preloading screen
     */
    $(window).ready(function() {
        setTimeout(function(){
            $(preloaderEl).fadeOut('slow', function () {
                $(this).remove();
            });
        }, 3000);
    });

})(jQuery);
