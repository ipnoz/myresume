<?php

if (!is_file('../env.local.php') OR !is_readable('../env.local.php')) {
    die('error');
}

require '../env.local.php';

define('MAIL_EOL', "\r\n");

/**
 * HTML submission validator
 */
if (! isset($_POST['email']) OR ! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    die('Erreur: veuillez entrer un email valide');
}
if (
    !isset(
        $_POST['cmd'],
        $_POST['name'],
        $_POST['telephone'],
        $_POST['subject'],
        $_POST['message']
    )
    OR $_POST['cmd'] !== 'send_email_contact'
    OR $_POST['name'] === ''
    OR $_POST['email'] === ''
    OR $_POST['message'] === ''
) {
    die('error');
}

$name = strip_tags($_POST['name']);
$email_address = strip_tags($_POST['email']);
$telephone = strip_tags($_POST['telephone']);
$subject = strip_tags($_POST['subject']);
$message = strip_tags($_POST['message']);

/**
 * Create the email and send the message
 */
$headers = 'From: '.$email_address.MAIL_EOL;
$headers .= 'Content-Type: text/plain; charset="UTF-8"'.MAIL_EOL;
$email_subject = 'Message depuis '.$_SERVER['HTTP_HOST']. ($subject ? ': '.$subject : '');

$email_body = 'De: '.$name.MAIL_EOL;
$email_body .= 'Téléphone: '.$telephone.MAIL_EOL;
$email_body .= 'Sujet: '.$subject.MAIL_EOL.MAIL_EOL;
$email_body .= $message.MAIL_EOL;

$result = mail(
    APP_CONTACT_EMAIL,
    $email_subject,
    $email_body,
    $headers
);

/**
 * return mail status
 */
die($result ? 'success' : 'error');
