#!/bin/bash

cd $(dirname "$0") || exit 1

source "./.env.local" || exit 1

OPTIONS="-v --update --delete --delete-before --times --recursive --compress --partial --progress --stats"

# Dir=755, Files=644
CHMOD="--perms --chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r"

# ##########
#
# PROCESS
#
# ##########

# Setup debug mode
if [ $APP_DEPLOY_DEBUG != 0 ]; then
	OPTIONS+=" --dry-run"
	echo
    echo "mode debug activé"
    echo
fi

rsync -e ssh $OPTIONS $CHMOD $APP_DEPLOY_DIR_LOCAL $APP_DEPLOY_DIR_REMOTE
